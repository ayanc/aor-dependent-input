'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.mapStateToProps = exports.DependentFieldComponent = undefined;

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _lodash = require('lodash.get');

var _lodash2 = _interopRequireDefault(_lodash);

var _lodash3 = require('lodash.set');

var _lodash4 = _interopRequireDefault(_lodash3);

var _FormField = require('admin-on-rest/lib/mui/form/FormField');

var _FormField2 = _interopRequireDefault(_FormField);

var _getValue = require('./getValue');

var _getValue2 = _interopRequireDefault(_getValue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DependentFieldComponent = function DependentFieldComponent(_ref) {
    var children = _ref.children,
        show = _ref.show,
        dependsOn = _ref.dependsOn,
        value = _ref.value,
        resolve = _ref.resolve,
        props = (0, _objectWithoutProperties3.default)(_ref, ['children', 'show', 'dependsOn', 'value', 'resolve']);

    if (!show) {
        return null;
    }

    if (Array.isArray(children)) {
        return _react2.default.createElement(
            'div',
            null,
            _react2.default.Children.map(children, function (child) {
                return _react2.default.createElement(
                    'div',
                    {
                        key: child.props.source,
                        style: child.props.style,
                        className: 'aor-input-' + child.props.source
                    },
                    _react2.default.createElement(_FormField2.default, (0, _extends3.default)({ input: child }, props))
                );
            })
        );
    }

    return _react2.default.createElement(
        'div',
        { key: children.props.source, style: children.props.style, className: 'aor-input-' + children.props.source },
        _react2.default.createElement(_FormField2.default, (0, _extends3.default)({ input: children }, props))
    );
};

exports.DependentFieldComponent = DependentFieldComponent;
DependentFieldComponent.propTypes = {
    children: _propTypes2.default.node.isRequired,
    dependsOn: _propTypes2.default.any,
    record: _propTypes2.default.object,
    resolve: _propTypes2.default.func,
    show: _propTypes2.default.bool.isRequired,
    value: _propTypes2.default.any
};

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state, _ref2) {
    var record = _ref2.record,
        resolve = _ref2.resolve,
        dependsOn = _ref2.dependsOn,
        value = _ref2.value;

    if (resolve && (dependsOn === null || typeof dependsOn === 'undefined')) {
        return { show: resolve(record, dependsOn, value) };
    }

    if (resolve && !Array.isArray(dependsOn)) {
        return { show: resolve((0, _getValue2.default)(record, dependsOn)) };
    }

    if (resolve && Array.isArray(dependsOn)) {
        var obj = dependsOn.reduce(function (acc, path) {
            var value = (0, _lodash2.default)(record, path);
            return (0, _lodash4.default)(acc, path, value);
        }, {});
        return { show: resolve(obj) };
    }

    if (Array.isArray(dependsOn) && Array.isArray(value)) {
        return {
            show: dependsOn.reduce(function (acc, s, index) {
                return acc && (0, _getValue2.default)(record, s) === value[index];
            }, true)
        };
    }

    if (typeof value === 'undefined') {
        if (Array.isArray(dependsOn)) {
            return {
                show: dependsOn.reduce(function (acc, s) {
                    return acc && !!(0, _getValue2.default)(record, s);
                }, true)
            };
        }

        return { show: !!(0, _getValue2.default)(record, dependsOn) };
    }

    return { show: (0, _getValue2.default)(record, dependsOn) === value };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)(DependentFieldComponent);